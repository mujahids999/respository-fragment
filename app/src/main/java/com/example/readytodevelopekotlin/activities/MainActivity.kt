package com.example.readytodevelopekotlin.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.readytodevelopekotlin.R
import com.example.readytodevelopekotlin.fragments.SplashFragment
import com.example.readytodevelopekotlin.utils.CommonMethods
import com.example.readytodevelopekotlin.utils.Singleton
import com.example.readytodevelopekotlin.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Singleton.context=this

        goForNextScreen()
    }

    private fun goForNextScreen() {
        CommonMethods.callFragment(SplashFragment.newInstance(),
            binding.flFragmentContainer.id,
            R.anim.fade_in, R.anim.fade_out,
            this, false)
    }


}