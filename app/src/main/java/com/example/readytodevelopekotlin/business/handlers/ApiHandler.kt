package com.example.readytodevelopekotlin.business.handlers

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.example.readytodevelopekotlin.business.interfaces.OnQuizLoadListener
import com.example.readytodevelopekotlin.business.interfaces.OnServerResultNotifier
import org.json.JSONObject

class ApiHandler {
    companion object{
        fun callGetApi(amount:Int,onQuizLoadListener:OnQuizLoadListener){
            var url:String="https://opentdb.com/api.php?amount=${amount}"
            CallForServer(url, object : OnServerResultNotifier {
                override fun onServerResultNotifier(isError: Boolean, response: String?) {

//                CommonMethods.hideProgressDialog();
                    if (!isError) {
                        onQuizLoadListener.onSuccess(response)
                    } else {
                        onQuizLoadListener.onError(response)
                    }
                }
            }).callForServerGet("quizApi",Priority.LOW)
        }

        fun callGetApiParamerters(cat: Int?,diff: String?,type: String?,onQuizLoadListener:OnQuizLoadListener){


//            var url:String="https://opentdb.com/api.php?amount=${amount}"

            var url: String ="https://opentdb.com/api.php?amount=10"


            if(cat!= null)
            {

                url += "&category=$cat"

            }

            if(diff != null)
            {
                url += "&difficulty=$diff"
            }


            if(type != null)
            {
                url += "&type=$type"
            }


            CallForServer(url, object : OnServerResultNotifier {
                override fun onServerResultNotifier(isError: Boolean, response: String?) {

//                CommonMethods.hideProgressDialog();
                    if (!isError) {
                        onQuizLoadListener.onSuccess(response)
                    } else {
                        onQuizLoadListener.onError(response)
                    }
                }
            }).callForServerGet("quizApi",Priority.LOW)
        }





//
//        fun networkLibrary( cat: Int?, diff: String?, type:String?) {
//
//
//            var url: String ="https://opentdb.com/api.php?amount=10"
//
//
//            if(cat!= null)
//            {
//
//                url += "&category=$cat"
//
//            }
//
//            if(diff != null)
//            {
//                url += "&difficulty=$diff"
//            }
//
//
//            if(type != null)
//            {
//                url += "&type=$type"
//            }
//
//
//
//            AndroidNetworking.get(url)
//                .setPriority(Priority.MEDIUM)
//                .build()
//                .getAsJSONObject(object : JSONObjectRequestListener {
//                    override fun onResponse(response: JSONObject?) {
//
////                        binding.loading.visibility = View.GONE
////                    hideCustomDialog()
//                        val intent = Intent(this@Gamemode, Attempt::class.java)
//                        intent.putExtra("Data" ,response.toString())
//                        intent.putExtra("Category",spincategory)
//                        startActivity(intent)
//                        finish()
//                    }
//
//                    override fun onError(anError: ANError?) {
//
////                    hideCustomDialog()
//                        Toast.makeText(this@Gamemode,"Error: ${anError?.message}", Toast.LENGTH_SHORT).show()
//                        Log.e("TAG", "onError: $anError" )
//                    }
//
//                })
//
//        }

    }
}