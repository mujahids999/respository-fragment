package com.example.readytodevelopekotlin.business.handlers

import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.StringRequestListener
import com.example.readytodevelopekotlin.business.interfaces.OnServerResultNotifier
import com.example.readytodevelopekotlin.utils.CommonMethods
import com.example.readytodevelopekotlin.utils.Singleton
import okhttp3.OkHttpClient
import java.util.HashMap
import java.util.concurrent.TimeUnit

class CallForServer {
    private var url: String? = null
    private var onServerResultNotifier: OnServerResultNotifier? = null
    private var paramStrings: Map<String, String> = HashMap()
    private val NO_INTERNET = "No internet connection"
    private val BASE_URL = ""
    private val API_BASE_URL = BASE_URL //+"api/";

    companion object{
        fun cancelRequest(tag: String?, cancelAll:Boolean){
            if(cancelAll){
                AndroidNetworking.cancelAll()
            }else{
                tag?.let{
                    AndroidNetworking.cancel(it)
                }
            }

        }
    }
    constructor(
        url: String?,
        onServerResultNotifier: OnServerResultNotifier?,
        paramStrings: Map<String, String>
    ) {
        this.url = url
        this.onServerResultNotifier = onServerResultNotifier
        this.paramStrings = paramStrings
    }

    constructor(url: String?, onServerResultNotifier: OnServerResultNotifier?) {
        this.url = url
        this.onServerResultNotifier = onServerResultNotifier
    }

    //Load data from server
    fun callForServerGet(tag:String, priority:Priority) {
        if (CommonMethods.isNetworkAvailable(Singleton.context!!)) {
            AndroidNetworking.initialize(Singleton.context)
            val client = OkHttpClient()
                .newBuilder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build()
            AndroidNetworking.initialize(Singleton.context, client)
            val getRequestBuilder = AndroidNetworking.get(API_BASE_URL + url)
            Log.e("Service_URL", url!!)

            getRequestBuilder.addHeaders(getHeaders())
            getRequestBuilder
                .setTag(tag)
                .setPriority(priority)
                .build().getAsString(object : StringRequestListener {
                override fun onResponse(jsonStr: String) {
                    onServerResultNotifier!!.onServerResultNotifier(false, jsonStr)
                    Log.e("ServiceResponse", "" + jsonStr)
                }

                override fun onError(error: ANError) {
                    var err = "Unable to get data from server"
                    if (error.errorBody != null) {
                        err = error.errorBody
                    }
                    onServerResultNotifier!!.onServerResultNotifier(true, err)
                    Log.e("ServiceError ", "" + err)
                }
            })
        } else {
            onServerResultNotifier!!.onServerResultNotifier(true, NO_INTERNET)
            Log.e("No Internet ", "" + NO_INTERNET)
        }
    }

    fun getHeaders(): Map<String, String>{
        val headers:HashMap<String, String> =HashMap()
        headers.put("Accept", "application/json")

        return headers
    }


}