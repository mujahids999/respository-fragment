package com.example.readytodevelopekotlin.business.models


class QUESTION() {

    var id: Int = 0
    var type: String= ""
    var difficulty: String =""
    var incorrect_answers: ArrayList<String> = ArrayList()
    var category: String =""
    var question:String= ""
    var correct_answer:String= ""
}