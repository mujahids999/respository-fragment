package com.arhamsoft.online.quizapp.adapters

import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.readytodevelopekotlin.R
import com.example.readytodevelopekotlin.databinding.AttemptViewBinding
import com.example.readytodevelopekotlin.roomDB.PlayTask

class RVAdapterAQ(private var qList: List<PlayTask>
) : RecyclerView.Adapter<RVAdapterAQ.ViewHolder>()  {


    private lateinit var binding: AttemptViewBinding


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = AttemptViewBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return ViewHolder(binding)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = qList[position]
        holder.bind.qname.text =  htmlConvert("Question: ${model.question}")
        holder.bind.tvattemptedAns.text = htmlConvert(model.attemptedAns)
        holder.bind.tvcorrectAns.text = htmlConvert(model.correctAns)

        if(qList[position].questionState == false)
        {
            holder.bind.ivTickCross.setImageResource(R.drawable.ic_baseline_check_false)

        }
        else
        {
            holder.bind.ivTickCross.setImageResource(R.drawable.ic_baseline_check_true_24)
        }

    }

    override fun getItemCount(): Int = qList.size

    class ViewHolder(val bind: AttemptViewBinding) : RecyclerView.ViewHolder(bind.root) {

    }


    fun htmlConvert(list: String): CharSequence? {

        if (Build.VERSION.SDK_INT >= 24)
        {
            return HtmlCompat.fromHtml(list, HtmlCompat.FROM_HTML_MODE_LEGACY);



        }
        else
        {
            return HtmlCompat.fromHtml(list, HtmlCompat.FROM_HTML_MODE_LEGACY);

        }


    }


}