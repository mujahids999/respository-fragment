package com.arhamsoft.online.quizapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import com.example.readytodevelopekotlin.databinding.CategoryViewBinding
import com.example.readytodevelopekotlin.roomDB.TaskDatabase


class RVAdapter(private var category: ArrayList<String>, private var point: Int
) : RecyclerView.Adapter<RVAdapter.ViewHolder>()  {


    private lateinit var binding: CategoryViewBinding
    private var mContext:Context?=null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = CategoryViewBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        mContext=parent.context
        return ViewHolder(binding)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var pt: Int =0
        val cc = category[position]
        holder.bind.cname.text =  cc

        val th = Thread(Runnable {
            mContext?.let {
                pt = TaskDatabase.getDatabase(it).playDao().getPoint(cc)
                holder.bind.tvpoint.text = "${pt} Points"
            }

        })
        th.start()
        th.join()


    }

    override fun getItemCount(): Int = category.size

    class ViewHolder(val bind: CategoryViewBinding) : RecyclerView.ViewHolder(bind.root) {

    }


}