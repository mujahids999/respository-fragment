package com.example.readytodevelopekotlin.roomDB

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "play_table")
class PlayTask {

    @PrimaryKey(autoGenerate = true)

    var id: Int = 0

    var question: String = ""

    var difficulty: String = ""

    var point: Int = 0

    var correctAns: String = ""

    var attemptedAns: String = ""

    var questionCategory: String = ""

    var questionState: Boolean = false
}