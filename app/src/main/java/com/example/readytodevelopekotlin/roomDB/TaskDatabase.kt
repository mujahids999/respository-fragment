package com.example.readytodevelopekotlin.roomDB




import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [PlayTask::class], version = 1 )

abstract   class TaskDatabase: RoomDatabase() {


    abstract fun playDao(): PlayDAO


    companion object{
        @Volatile

        private  var INSTANACE: TaskDatabase? = null

        fun getDatabase(context: Context): TaskDatabase
        {
            if(INSTANACE == null){
                synchronized(this){

                    INSTANACE = Room.databaseBuilder(
                        context.applicationContext,
                        TaskDatabase::class.java,
                        "AppDB"
                    ).build()
                }


            }
            return INSTANACE!!
        }


    }


}