package com.example.readytodevelopekotlin.roomDB

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PlayDAO {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertQ(playTask: PlayTask)

    @Query("SELECT * FROM play_table")
    fun getAllQues(): List<PlayTask>

    @Query("SELECT SUM(point) FROM play_table WHERE questionCategory =:category ")
    fun getPoint(category: String):Int

    @Query("SELECT COUNT(questionState) FROM play_table  WHERE questionState==1")
    fun getCorrectAns(): Int

    @Query("SELECT COUNT(questionState) FROM play_table  WHERE questionState==0")
    fun getWrongAns(): Int

    @Query("SELECT SUM(point) FROM play_table WHERE questionState == 1 ")
    fun getNoOfPoint():Int



}