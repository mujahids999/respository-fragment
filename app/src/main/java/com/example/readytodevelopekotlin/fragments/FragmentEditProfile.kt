package com.example.readytodevelopekotlin.fragments
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.readytodevelopekotlin.R
import com.example.readytodevelopekotlin.databinding.ActivityDashboardBinding
import com.example.readytodevelopekotlin.databinding.ActivityEditProfileBinding
import com.example.readytodevelopekotlin.preferences.PreferencesUtils
import com.example.readytodevelopekotlin.utils.CommonMethods


class FragmentEditProfile: BaseFragment() {


    lateinit var binding: ActivityEditProfileBinding
    var name:String? = null

    companion object {
        fun newInstance(): FragmentEditProfile {
            return FragmentEditProfile()
        }
    }
    override fun initView() {

        retreiveData()

        binding.backtoprofile.setOnClickListener {

            mActivity.supportFragmentManager.popBackStackImmediate()

        }



        binding.update.setOnClickListener {


            name = binding.etsetname.text.toString()

            PreferencesUtils(mActivity).putString("user_name", name)

            mActivity.supportFragmentManager.popBackStackImmediate()

        }

    }

    override fun loadData() {

    }

    override val fragmentTag: String?
        get() = FragmentEditProfile::class.java.simpleName


    @Nullable
    @Override
    override fun onCreateView(@NonNull inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {


        binding = ActivityEditProfileBinding.inflate(LayoutInflater.from(context))



        return binding.root


    }

    fun retreiveData(){

        binding.etsetname.setText(PreferencesUtils(mActivity).getString("user_name",""))

    }










}