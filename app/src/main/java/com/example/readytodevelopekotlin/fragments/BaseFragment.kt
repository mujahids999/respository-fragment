package com.example.readytodevelopekotlin.fragments

import android.annotation.SuppressLint
import androidx.fragment.app.FragmentActivity
import com.example.readytodevelopekotlin.fragments.BaseFragment
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import com.example.readytodevelopekotlin.databinding.EmptyFragmentBinding
import com.example.readytodevelopekotlin.utils.CommonMethods

/**
 * A simple [Fragment] subclass.
 *
 * Created by Rana Zeshan on 22-Nov-2021.
 */
@SuppressLint("ValidFragment")
abstract class BaseFragment : Fragment() {
    protected lateinit var mActivity: FragmentActivity
//    protected var mView: View? = null



    companion object {
        @SuppressLint("StaticFieldLeak")
        private var ibBack: Button? = null

        fun setIbBack(ibBack: Button?) {
            Companion.ibBack = ibBack
        }

        fun getIbBack(): Button? {
            return ibBack
        }

        private var binding:Any?=null
        fun setBinding(binding:Any){
            Companion.binding=binding
        }
    }

    protected abstract fun initView()
    protected abstract fun loadData()
    abstract val fragmentTag: String?

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        mView = view
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    CommonMethods.pgDialog?.let {
                        if(it.isShowing){
                            return@OnKeyListener false
                        }
                    }

                    if (mActivity!!.supportFragmentManager.backStackEntryCount == 0) {
                        return@OnKeyListener false
                    }
                    if (ibBack == null) {
                        mActivity!!.supportFragmentManager.popBackStack()
                    } else {
                        ibBack!!.performClick()
                        ibBack = null
                    }
                    return@OnKeyListener true
                }
            }
            false
        })
        initView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = requireActivity()
    }

    override fun onResume() {
        super.onResume()
        loadData()

        ibBack?.setOnClickListener {
            mActivity!!.supportFragmentManager.popBackStackImmediate()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}