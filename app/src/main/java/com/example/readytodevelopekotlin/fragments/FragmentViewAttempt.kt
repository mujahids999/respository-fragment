package com.example.readytodevelopekotlin.fragments
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.arhamsoft.online.quizapp.adapters.RVAdapterAQ
import com.example.readytodevelopekotlin.databinding.ActivityDashboardBinding
import com.example.readytodevelopekotlin.databinding.ActivityViewAttemptQuesBinding
import com.example.readytodevelopekotlin.roomDB.PlayTask
import com.example.readytodevelopekotlin.roomDB.TaskDatabase


class FragmentViewAttempt: BaseFragment() {


    lateinit var binding: ActivityViewAttemptQuesBinding
    private var tList: List<PlayTask> = ArrayList()

    companion object {
        fun newInstance(): FragmentViewAttempt {
            return FragmentViewAttempt()
        }
    }
    override fun initView() {

        binding.backtodashboard.setOnClickListener {

            mActivity.supportFragmentManager.popBackStackImmediate()
        }

        val th = Thread(Runnable {
            tList = TaskDatabase.getDatabase(mActivity).playDao().getAllQues()

        })
        th.start()
        th.join()

        setData()

    }

    override fun loadData() {

    }

    override val fragmentTag: String?
        get() = FragmentViewAttempt::class.java.simpleName


    @Nullable
    @Override
    override fun onCreateView(@NonNull inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {


        binding = ActivityViewAttemptQuesBinding.inflate(LayoutInflater.from(context))



        return binding.root


    }


    fun setData()
    {
        binding.recycleListAQ.layoutManager = LinearLayoutManager(
            mActivity,
            LinearLayoutManager.VERTICAL, false
        )

        binding.recycleListAQ.adapter = RVAdapterAQ(tList)
    }









}