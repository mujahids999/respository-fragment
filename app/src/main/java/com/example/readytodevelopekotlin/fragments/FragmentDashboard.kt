package com.example.readytodevelopekotlin.fragments

import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.readytodevelopekotlin.R
import com.example.readytodevelopekotlin.databinding.ActivityDashboardBinding
import com.example.readytodevelopekotlin.preferences.PreferencesUtils
import com.example.readytodevelopekotlin.roomDB.TaskDatabase
import com.example.readytodevelopekotlin.utils.CommonMethods


class FragmentDashboard : BaseFragment() {


    lateinit var binding: ActivityDashboardBinding
    private var totalpt: Int = 0


    companion object {
        fun newInstance(): FragmentDashboard {
            return FragmentDashboard()
        }
    }

    override fun initView() {

        val th4 = Thread(Runnable {
            totalpt = TaskDatabase.getDatabase(mActivity).playDao().getNoOfPoint()
        })
        th4.start()
        th4.join()

        binding.tvshowpoints.text = "${totalpt.toString()} Points"
        binding.tvattempt.text = "Attempts (${PreferencesUtils(mActivity).getInt("points", 0)})"

        binding.gotogamemode.setOnClickListener {

            goToGamemode()
        }

        binding.attemptques.setOnClickListener {
            goToViewattempt()
        }

        binding.viewpoints.setOnClickListener {

            goToViewpoints()
        }

        binding.stats.setOnClickListener {

            goToStatistics()
        }

        binding.profile.setOnClickListener {

            goToProfile()
        }

        binding.setting.setOnClickListener {

            settingDialog()

        }


    }

    override fun loadData() {

    }

    override val fragmentTag: String?
        get() = FragmentDashboard::class.java.simpleName


    @Nullable
    @Override
    override fun onCreateView(
        @NonNull inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        binding = ActivityDashboardBinding.inflate(LayoutInflater.from(context))

        setBinding(binding)

        return binding.root


    }


    private fun goToGamemode() {
        CommonMethods.callFragment(
            FragmentGamemode.newInstance(),
            R.id.flFragmentContainer,
            R.anim.slide_in_right,
            R.anim.slide_out_left,
            mActivity,
            true
        )
    }


    private fun goToViewattempt() {
        CommonMethods.callFragment(
            FragmentViewAttempt.newInstance(),
            R.id.flFragmentContainer,
            R.anim.slide_in_right,
            R.anim.slide_out_left,
            mActivity,
            true
        )
    }

    private fun goToViewpoints() {
        CommonMethods.callFragment(
            FragmentViewPoints.newInstance(),
            R.id.flFragmentContainer,
            R.anim.slide_in_right,
            R.anim.slide_out_left,
            mActivity,
            true
        )
    }

    private fun goToStatistics() {
        CommonMethods.callFragment(
            FragmentStatistics.newInstance(),
            R.id.flFragmentContainer,
            R.anim.slide_in_right,
            R.anim.slide_out_left,
            mActivity,
            true
        )
    }
 
    private fun goToProfile() {
        CommonMethods.callFragment(
            FragmentProfile.newInstance(),
            R.id.flFragmentContainer,
            R.anim.slide_in_right,
            R.anim.slide_out_left,
            mActivity,
            true
        )
    }

    override fun onResume() {
        super.onResume()
        retreiveData()
    }


    fun settingDialog() {

        var alertDialog = AlertDialog.Builder(mActivity)


        alertDialog
            .setMessage("Settings are not available right now.")
            .setCancelable(false)
            .setPositiveButton("ok", DialogInterface.OnClickListener { dialogInterface, which ->

                dialogInterface.dismiss()

            })

        alertDialog.create().show()

    }

    fun retreiveData() {

        binding.tvshowname.text = PreferencesUtils(mActivity).getString("user_name", "You")

    }


}