package com.example.readytodevelopekotlin.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.readytodevelopekotlin.business.handlers.ApiHandler
import com.example.readytodevelopekotlin.business.interfaces.OnQuizLoadListener
import com.example.readytodevelopekotlin.databinding.EmptyFragmentBinding
import com.example.readytodevelopekotlin.utils.CommonMethods

class EmptyFragment : BaseFragment() {
    private lateinit var binding: EmptyFragmentBinding

    companion object {
        fun newInstance(): EmptyFragment {
            return EmptyFragment()
        }
    }

    override fun initView() {
        CommonMethods.setupUI(binding.root, mActivity)
    }

    override fun loadData() {
        CommonMethods.showProgressDialog(mActivity)
        ApiHandler.callGetApi(10,object : OnQuizLoadListener{
            override fun onSuccess(response: String?) {
                CommonMethods.hideProgressDialog()
//                binding.tvResponse.text="Success: ${response}"
            }

            override fun onError(error: String?) {
                CommonMethods.hideProgressDialog()
//                binding.tvResponse.text="Success: ${error}"

            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = EmptyFragmentBinding.inflate(layoutInflater)
        setBinding(binding)
        return binding.root
    }

    override val fragmentTag: String
        get() = EmptyFragment::class.java.simpleName

}