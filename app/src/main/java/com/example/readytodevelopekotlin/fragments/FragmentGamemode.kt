package com.example.readytodevelopekotlin.fragments
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.example.readytodevelopekotlin.R
import com.example.readytodevelopekotlin.business.handlers.ApiHandler
import com.example.readytodevelopekotlin.business.interfaces.OnQuizLoadListener
import com.example.readytodevelopekotlin.business.models.MyData
import com.example.readytodevelopekotlin.business.models.QUESTION
import com.example.readytodevelopekotlin.databinding.ActivityGamemodeBinding
import com.example.readytodevelopekotlin.preferences.PreferencesUtils
import com.example.readytodevelopekotlin.utils.CommonMethods
import com.google.gson.Gson
import org.json.JSONObject


class FragmentGamemode: BaseFragment() {


    lateinit var binding: ActivityGamemodeBinding
    var spincategory:Int = 0
    var selectedCategory: Int? = null
    var selectedDifficulty:String? = null
    var selectedType:String? = null
    var list: List<QUESTION> = ArrayList()


    companion object {
        fun newInstance(): FragmentGamemode {
            return FragmentGamemode()
        }
    }

    override fun initView() {
        categorySpinner()
        levelSpinner()
        choiceSpinner()

        binding.backtodashboard.setOnClickListener {

            mActivity.supportFragmentManager.popBackStackImmediate()

        }


    }

    override fun loadData() {


        binding.start.setOnClickListener {
            ApiHandler.callGetApiParamerters(selectedCategory,selectedDifficulty,selectedType, object: OnQuizLoadListener{
                override fun onSuccess(response: String?) {
                CommonMethods.hideProgressDialog()


                    convertIntoList(response.toString())
                    Log.e("api se", response.toString())
                    goToPlay()

                }

                override fun onError(error: String?) {
                    CommonMethods.hideProgressDialog()
                    Toast.makeText(mActivity,"Api Error ${error.toString()} ",Toast.LENGTH_LONG).show()
                }

            } )

            CommonMethods.showProgressDialog(mActivity)

            addAttempt()
        }
    }

    override val fragmentTag: String?
        get() = FragmentGamemode::class.java.simpleName


    @Nullable
    @Override
    override fun onCreateView(@NonNull inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {

        binding = ActivityGamemodeBinding.inflate(LayoutInflater.from(context))
        return binding.root


    }


    private fun goToPlay() {
        CommonMethods.callFragment(FragmentPlay.newInstance(list,spincategory), R.id.flFragmentContainer, R.anim.slide_in_right, R.anim.slide_out_left, mActivity, true)
    }


    fun categorySpinner()
    {

        var spinAdapter = ArrayAdapter.createFromResource(
            mActivity,
            R.array.catego,
            android.R.layout.simple_spinner_item
        )

        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.spinner.adapter = spinAdapter

        binding.spinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, positionc: Int, id: Long)  {


                if(positionc == 0)
                {
                    selectedCategory= positionc
                    spincategory=0
                }
                else
                {
                    selectedCategory=positionc+8
                    spincategory=positionc
                }
                Log.e("value =", selectedCategory.toString())

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        })
    }


    fun levelSpinner()
    {
        var spinAdapter2 = ArrayAdapter.createFromResource(
            mActivity,
            R.array.difficulty,
            android.R.layout.simple_spinner_item
        )

        spinAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.spinner2.adapter = spinAdapter2

        binding.spinner2.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, positiond: Int, id: Long)  {

                if(positiond == 0)
                {
                    selectedDifficulty= ""
                }
                else if(positiond == 1 )
                {
                    selectedDifficulty = "easy"
                }
                else if(positiond == 2 )
                {
                    selectedDifficulty = "medium"
                }
                else if(positiond == 3 )
                {
                    selectedDifficulty = "hard"
                }

                Log.e("value =", selectedDifficulty.toString())

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        })

    }

    fun choiceSpinner()
    {

        var spinAdapter3 = ArrayAdapter.createFromResource(
            mActivity,
            R.array.type,
            android.R.layout.simple_spinner_item

        )

        spinAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.spinner3.adapter = spinAdapter3

        binding.spinner3.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, positiont: Int, id: Long)  {
//                val spin_id: String = binding.spinner.getSelectedItem().toString()


                if(positiont == 0)
                {
                    selectedType= ""
                }
                else if(positiont == 1 )
                {
                    selectedType = "multiple"
                }
                else if(positiont == 2 )
                {
                    selectedType = "boolean"
                }

                Log.e("value =", selectedType.toString())

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        })
    }


    fun addAttempt(){

        PreferencesUtils(mActivity).putInt("points",(PreferencesUtils(mActivity).getInt("points",0)+1))

    }

    fun convertIntoList(response: String?) {
        val gson = Gson()
        val obj3 = gson.fromJson(response, MyData::class.java)

        list = obj3.results

        Log.e("response from api", list.toString() )
    }













}