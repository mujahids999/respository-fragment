package com.example.readytodevelopekotlin.fragments
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.readytodevelopekotlin.R
import com.example.readytodevelopekotlin.databinding.ActivityDashboardBinding
import com.example.readytodevelopekotlin.databinding.ActivityProfileBinding
import com.example.readytodevelopekotlin.preferences.PreferencesUtils
import com.example.readytodevelopekotlin.roomDB.PlayTask
import com.example.readytodevelopekotlin.roomDB.TaskDatabase
import com.example.readytodevelopekotlin.utils.CommonMethods


class FragmentProfile: BaseFragment() {


    lateinit var binding: ActivityProfileBinding
    private var totalca: Int =0
    private var totalwa: Int =0
    private var totalpt: Int =0
    private var tList: List<PlayTask> = ArrayList()

    companion object {
        fun newInstance(): FragmentProfile {
            return FragmentProfile()
        }
    }
    override fun initView() {


        binding.backtodashboard.setOnClickListener {

            mActivity.supportFragmentManager.popBackStackImmediate()

        }

        binding.ivedit.setOnClickListener {

            goToEditProfile()
        }


        val th = Thread(Runnable {
            totalca = TaskDatabase.getDatabase(mActivity).playDao().getCorrectAns()

        })
        th.start()
        th.join()

        val th2 = Thread(Runnable {
            totalwa = TaskDatabase.getDatabase(mActivity).playDao().getWrongAns()

        })
        th2.start()
        th2.join()

        val th3 = Thread(Runnable {
            tList = TaskDatabase.getDatabase(mActivity).playDao().getAllQues()

        })
        th3.start()
        th3.join()

        val th4 = Thread(Runnable {
            totalpt = TaskDatabase.getDatabase(mActivity).playDao().getNoOfPoint()

        })
        th4.start()
        th4.join()


        binding.tvcorrect.text = totalca.toString()
        binding.tvincorrect.text = totalwa.toString()
        binding.tvtotalques.text = tList.size.toString()
        binding.tvtotalpt.text = totalpt.toString()

        binding.tvshowpoints.text = "${totalpt.toString()} Points"

        binding.tvattempt.text = "Attempts (${PreferencesUtils(mActivity).getInt("points",0)})"


    }

    override fun loadData() {

    }

    override val fragmentTag: String?
        get() = FragmentProfile::class.java.simpleName


    @Nullable
    @Override
    override fun onCreateView(@NonNull inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {


        binding = ActivityProfileBinding.inflate(LayoutInflater.from(context))



        return binding.root


    }


    private fun goToEditProfile() {
        CommonMethods.callFragment(FragmentEditProfile.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, mActivity, true)
    }



    override fun onResume() {
        super.onResume()
        retreiveData()
    }

    fun retreiveData(){

        binding.tvshowname.text = PreferencesUtils(mActivity).getString("user_name","You")

    }









}