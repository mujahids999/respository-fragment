package com.example.readytodevelopekotlin.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.readytodevelopekotlin.R
import com.example.readytodevelopekotlin.databinding.EmptyFragmentBinding
import com.example.readytodevelopekotlin.databinding.SplashFragmentBinding
import com.example.readytodevelopekotlin.fragments.SplashFragment.Companion.newInstance
import com.example.readytodevelopekotlin.utils.CommonMethods

/**
 * Created by Rana Zeshan on 22-Nov-19.
 */
class SplashFragment : BaseFragment() {
    private lateinit var binding: SplashFragmentBinding

    companion object {
        fun newInstance(): SplashFragment {
            return SplashFragment()
        }
    }

    override fun initView() {
        CommonMethods.setupUI(binding.root, mActivity)
    }

    override fun loadData() {
//        CommonMethods.showProgressDialog(mActivity)
        Handler(Looper.getMainLooper()).postDelayed(Runnable { goToHomeScreen() },3000)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = SplashFragmentBinding.inflate(layoutInflater)
        setBinding(binding)
        return binding.root
    }

    override val fragmentTag: String
        get() = SplashFragment::class.java.simpleName

    private fun goToHomeScreen(){
//        CommonMethods.hideProgressDialog()

        CommonMethods.callFragment(FragmentDashboard.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, mActivity, false)
    }
}