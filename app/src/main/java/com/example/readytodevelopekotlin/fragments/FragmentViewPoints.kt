package com.example.readytodevelopekotlin.fragments
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.arhamsoft.online.quizapp.adapters.RVAdapter
import com.example.readytodevelopekotlin.databinding.ActivityDashboardBinding
import com.example.readytodevelopekotlin.databinding.ActivityGamemodeBinding
import com.example.readytodevelopekotlin.databinding.ActivityViewpointsBinding


class FragmentViewPoints: BaseFragment() {


    lateinit var binding: ActivityViewpointsBinding
    lateinit var catname: ArrayList<String>
    var pt: Int =0

    companion object {
        fun newInstance(): FragmentViewPoints {
            return FragmentViewPoints()
        }
    }
    override fun initView() {

        binding.backtodashboard.setOnClickListener {

            mActivity.supportFragmentManager.popBackStackImmediate()
        }

        setData()

    }

    override fun loadData() {


    }

    override val fragmentTag: String?
        get() = FragmentViewPoints::class.java.simpleName


    @Nullable
    @Override
    override fun onCreateView(@NonNull inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {


        binding = ActivityViewpointsBinding.inflate(LayoutInflater.from(context))



        return binding.root


    }


    fun setData()
    {
        binding.recycleList.layoutManager = LinearLayoutManager(
            mActivity,
            LinearLayoutManager.VERTICAL, false
        )

        catname = ArrayList()


        catname.add("Any Category")
        catname.add("General Knowledge")
        catname.add("Entertainment: Books")
        catname.add("Entertainment: Film")
        catname.add("Entertainment: Music")
        catname.add("Entertainment: Musicals & Theatres")
        catname.add("Entertainment: Television")
        catname.add("Entertainment: Video Games")
        catname.add("Entertainment: Board Games")
        catname.add("Science & Nature")
        catname.add("Science: Computers")
        catname.add("Science: Mathematics")
        catname.add("Mythology")
        catname.add("Sports")
        catname.add("Geography")
        catname.add("History")
        catname.add("Politics")
        catname.add("Art")
        catname.add("Celebrities")
        catname.add("Animals")
        catname.add("Vehicles")
        catname.add("Entertainment: Comics")
        catname.add("Science: Gadgets")
        catname.add("Entertainment: Japanese Anime & Manga")
        catname.add("Entertainment: Cartoon & Animations")


        binding.recycleList.adapter = RVAdapter( catname, pt )
    }











}