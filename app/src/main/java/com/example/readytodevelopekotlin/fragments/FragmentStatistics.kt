package com.example.readytodevelopekotlin.fragments
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.readytodevelopekotlin.databinding.ActivityDashboardBinding
import com.example.readytodevelopekotlin.databinding.ActivityStatisticsBinding
import com.example.readytodevelopekotlin.preferences.PreferencesUtils
import com.example.readytodevelopekotlin.roomDB.PlayTask
import com.example.readytodevelopekotlin.roomDB.TaskDatabase


class FragmentStatistics: BaseFragment() {


    lateinit var binding: ActivityStatisticsBinding
    private var totalca: Int =0
    private var totalwa: Int =0
    private var totalpt: Int =0
    private var tList: List<PlayTask> = ArrayList()
    companion object {
        fun newInstance(): FragmentStatistics {
            return FragmentStatistics()
        }
    }
    override fun initView() {

        binding.backtodashboard.setOnClickListener {

            mActivity.supportFragmentManager.popBackStackImmediate()

        }

        val th = Thread(Runnable {
            totalca = TaskDatabase.getDatabase(mActivity).playDao().getCorrectAns()

        })
        th.start()
        th.join()

        val th2 = Thread(Runnable {
            totalwa = TaskDatabase.getDatabase(mActivity).playDao().getWrongAns()

        })
        th2.start()
        th2.join()

        val th3 = Thread(Runnable {
            tList = TaskDatabase.getDatabase(mActivity).playDao().getAllQues()

        })
        th3.start()
        th3.join()

        val th4 = Thread(Runnable {
            totalpt = TaskDatabase.getDatabase(mActivity).playDao().getNoOfPoint()

        })
        th4.start()
        th4.join()


        binding.tvcorrect.text = totalca.toString()
        binding.tvincorrect.text = totalwa.toString()
        binding.tvtotalques.text = tList.size.toString()
        binding.tvtotalpt.text = totalpt.toString()
        binding.tvtotalatt.text = " ${PreferencesUtils(mActivity).getInt("points",0)}"
    }

    override fun loadData() {

    }

    override val fragmentTag: String?
        get() = FragmentStatistics::class.java.simpleName


    @Nullable
    @Override
    override fun onCreateView(@NonNull inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {


        binding = ActivityStatisticsBinding.inflate(LayoutInflater.from(context))



        return binding.root


    }












}